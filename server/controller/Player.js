const express = require('express');
const Player = require('../model/Player');
const underscore = require('underscore');
const app = express();

app.get('/player', async ( req , res ) => {
    try {
        let players = await Player.find({state: true}).exec()
        res.json({players})
    }catch (e){
        console.error(`Error in DB`)
        res.json({err:e,message:'Data Base Down'})
    }
})
app.post('/player', async ( req , res ) => {
    let body = req.body
    let player = new Player({
        name: body.name,
        position: body.position,
        username: body.username,
        password: body.password,
        numberOfGoals: body.numberOfGoals
    })
    try {
        let playerDB = await player.save();
        res.json({ok:true,player: playerDB})
    }catch (e){
        console.error(`Error in DB`)
        res.json({ok:false,message: 'Error in DB'})
    }
})
app.put('/player/:id', async ( req , res ) => {
    let idPlayer =req.params.id
    let body = req.body;
    try {
        let playerUpdate = await Player.findByIdAndUpdate(idPlayer,body,{new: true})
        res.json({ok:true,player: playerUpdate})
    }catch (e){
        console.error(`Error in DB`)
        res.json({ok:false,message: `Error in DB ${e}`})
    }
})
app.delete('/player/:id', async ( req , res ) => {
    let idPlayer = req.params.id
    let changeState = { state: false }
    try {
        let playerDelete = await Player.findByIdAndUpdate(idPlayer,changeState,{new: true})
        res.json({ok:true,player: playerDelete})
    }catch (e){
        console.error(`Error in DB`)
    }
})
app.post('/valid', async (req,res) => {
    let {username, password} = req.body;
    try {
        let player = await Player.find({username,password}).exec()
        if (player.length === 0){
            res.json({ok:false,message: `Not Authorized`})
        }
        res.json({ok:true,message: 'Authorized',player})
    }catch (e){
        console.error(`Error in DB`)
        res.json({ok:false,message: `Error in DB ${e}`})
    }
})
module.exports = app;