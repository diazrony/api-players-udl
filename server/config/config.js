//============================
// Port
//============================
process.env.PORT = process.env.PORT || 3001
//============================
// Enviroment
//============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'
//============================
// DataBase
//============================
let urlDataBase;

if ( process.env.NODE_ENV === 'dev' ) {
    urlDataBase = 'mongodb://localhost:27017/Players'
}else{
    urlDataBase = process.env.MONGO_URI
}

process.env.URLDB = urlDataBase