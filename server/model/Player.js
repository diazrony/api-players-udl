const mongoose = require('mongoose');
let Shema = mongoose.Schema;

let playerShema = new Shema({
    name:{
        type: String,
        required: [true, 'Name is necesary']
    },
    position: {
        type: String,
        required: [true, 'Position is necesary'],
    },
    username: {
        type: String,
        required: [true, 'username is necesary'],
    },
    password: {
        type: String,
        required: [true, 'Password is necesary']
    },
    numberOfGoals: {
        type: Number,
        default: 0
    },
    state: {
        type: Boolean,
        default: true
    },
},{timestamps: true} );

module.exports = mongoose.model('player', playerShema)